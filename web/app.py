from flask import Flask, render_template, request, abort

app = Flask(__name__)

@app.route("/<name>")
def hello(name):
    if name[-4:] == 'html':
        try:
            if '..' in name or '~' in name or '//' in name:
                return error_403(403)
            else:
                return render_template(name)
        except:
            return error_404(404)
    elif name[-3:] == 'css':
        try:
            if '..' in name or '~' in name or '//' in name:

                return error_403(403)
            else:
                return render_template(name)
        except:
            return error_404(404)

@app.errorhandler(404)
def error_404(error):
    return render_template('404.html'), 404

@app.errorhandler(403)
def error_403(error):
    return render_template('403.html'), 403

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
 
